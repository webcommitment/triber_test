Component({
  data: {
    siteUrl: 'www.triber.com.ph',
    socialMedia: [
      {
        link: 'https://facebook.com',
        iconUrl: '../../assets/facebook.svg'
      },
      {
        link: 'https://instagram.com',
        iconUrl: '../../assets/instagram.svg'
      },
      {
        link: 'https://twitter.com',
        iconUrl: '../../assets/twitter.svg'
      },
    ]
  },
});
