Component({
  props: {
    onClick: () => {},
  },
  methods: {
    onClick() {
      this.props.onClick();
    }
  },
  data: {
    slide: [
      {
        topImageUrl: '../../assets/intro/1-top.jpg',
        bottomImageUrl: '../../assets/intro/1.jpg',
        imageUrl: null,
        buttonText: null,
      },
      {
        topImageUrl: null,
        bottomImageUrl: null,
        imageUrl: '../../assets/intro/2.jpg',
        buttonText: null,
      },
      {
        topImageUrl: null,
        bottomImageUrl: null,
        imageUrl: '../../assets/intro/3.jpg',
        buttonText: 'START A GROUP BUY NOW',
      }
    ],
    indicatorDots: true,
    autoplay: false,
    current: null,
  },
  changeIndicatorDots(e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
});
