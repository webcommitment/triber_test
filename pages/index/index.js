// Get the global app instance
const app = getApp();

Page({
    // Declare page data
    data: {
        pageTitle: 'Front Page',
        currentPage: 'home',
        sliderViewed: false,
    },
    onTapStartButton() {
        const prevState = this.data.sliderViewed;
        this.setData({ sliderViewed: !prevState });
    },
    onLoad(query) {
        // Page load
        console.info(`Page onLoad with query: ${JSON.stringify(query)}`);
    },
    onShareAppMessage() {
        // Back to custom sharing information
        return {
            title: 'My App',
            desc: 'My App description',
            path: 'pages/index/index'
        };
    },
});
